﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SayHello : MonoBehaviour {

	public float delay;
	private float nextOutputTime;

	// Use this for initialization
	void Start () {
		nextOutputTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		//every x seconds
		if (Time.time >= nextOutputTime) {
			//say hello
			SayHi ();
			nextOutputTime = Time.time + delay;
		}
	}

	void SayHi () {
		Debug.Log ("Hello!");
	}

}
