﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempMover : MonoBehaviour {

	private Transform tf;
	public float speed;

	// Use this for initialization
	void Start () {
		tf = this.gameObject.GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		tf.position += (Vector3.right * speed);
	}
}
