﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayersStats : MonoBehaviour {

    [HideInInspector] public float firstPlayerHealth;
    [HideInInspector] public float firstPlayerScore;
    [HideInInspector] public float secondPlayerHealth;
    [HideInInspector] public float secondPlayerScore;
    [HideInInspector] public TankData p1data;
    [HideInInspector] public TankData p2data;


    [HideInInspector] public GameObject playerOne;
    [HideInInspector] public GameObject playerTwo;

    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        playerOne = GameObject.Find("PlayerTank One");
        p1data = playerOne.GetComponent<TankData>();
        p1data.health.currentHealth = firstPlayerHealth;
        p1data.scores.playerScore = firstPlayerScore;
        playerTwo = GameObject.Find("PlayerTank Two");
        p2data = playerTwo.GetComponent<TankData>();
        p2data.health.currentHealth = secondPlayerHealth;
        p2data.scores.playerScore = secondPlayerScore;
    }
}
