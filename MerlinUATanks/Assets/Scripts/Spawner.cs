﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    //list of added itms to the script such as lists and objects to use
    public GameObject objectToSpawn;
    public GameObject spawnedObject;
    private Transform tf;
    public float timeBetweenSpawns;
    private float spawnCountdown;
    public List<GameObject> powerUpPrefabs;

    // Use this for initialization
    void Start () {
        //gets the transform to use from the current object the script is on
        tf = GetComponent<Transform>();
        // generates a random powerup from the prefabs of available powerups
        //then names that prefab powerup and sets the object ot be spawned as that random powerup
        int rand = Random.Range(0, powerUpPrefabs.Count);
        GameObject newPowerUpPrefab = Instantiate(powerUpPrefabs[rand]) as GameObject;
        newPowerUpPrefab.name = "PowerUp";
        objectToSpawn = newPowerUpPrefab;
        //sets the countdown to the time between spawns so that it can always have a base number to refer to
        spawnCountdown = timeBetweenSpawns;

    }
	
	// Update is called once per frame
	void Update () {
        //if there is no spawned object do the following
		if (spawnedObject == null)
        {
            // count how long till the object will spawn
            spawnCountdown -= Time.deltaTime;
            //when the counter hits zero it will spawn the objects prefab and reset the timer till the object is picked up and then starts the process again
            if (spawnCountdown <= 0) {
                spawnedObject = Instantiate(objectToSpawn, tf.position, tf.rotation) as GameObject;

                spawnCountdown = timeBetweenSpawns;
           }
        }
	}
}
