﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScores : MonoBehaviour {

    public float playerScore;
    public Text scoreText;

    private TankData data;

    // Use this for initialization
    void Start () {
        data = GetComponent<TankData>();
        setScoreText();
    }
	
	// Update is called once per frame
	void Update () {
        setScoreText();
    }

    void setScoreText()
    {
        scoreText.text = "Score: " + playerScore.ToString();
    }


}
