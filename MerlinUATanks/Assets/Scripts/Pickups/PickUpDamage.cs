﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpDamage : MonoBehaviour {
    //points to the powerup that falls in line with the pickup
    public PowerUpDamage powerup;


    public AudioSource sfxAudio;
    public AudioClip pickUpSound;
    [HideInInspector] public GameObject gm;
    [HideInInspector] public GameManager gameMan;
    //triggers when the object is entered
    private void OnTriggerEnter(Collider other)
    {
        sfxAudio.PlayOneShot(pickUpSound, sfxAudio.volume);
        //sets the power up manager to pm and gets the tanks powerupmanager that ran into the powerup
        PowerupManager pm = other.GetComponent<PowerupManager>();
        //does something if there is a power up manager equiped
        if (pm != null)
        {
            //adds the powerup scripts effect to the tanks manager and then destroyes the object
            pm.AddPowerup(powerup);
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
        gm = GameObject.Find("GameManager");
        gameMan = gm.GetComponent<GameManager>();
        sfxAudio = gameMan.sfxAudio;
        pickUpSound = gameMan.audioClips[4];
    }

    // Update is called once per frame
    void Update()
    {

    }
}