﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ScoreData : MonoBehaviour//IComparable<ScoreDataUser>
{
    public float highScore;
    public float score;
    public string name;
    public float playerOneScore;
    public float playerTwoScore;
    //public ArrayList highScores;
    public List<float> highScores = new List<float>();
    //List<ScoreData> highScores = new List<ScoreData>();
    public Text highScore1Text;
    public Text highScore2Text;
    public Text highScore3Text;

    private GameObject gManager;
    [HideInInspector] public GameManager gm;

    //public int CompareTo(ScoreData score)
    //{
    //   if (other == null)
    //    {
    //        return 1;
    //    }
    //    if (this.score > other.score)
    //    {
    //        return 1;
    //    }
    //    if (this.score < other.score)
    //    {
    //        this.score = other.score;
    //
    //       return -1;
    //    }
    //   return 0;
    //}
    // ... somewhere in our code, we need to add all our high scores (and our player's score)
    // Use this for initialization
    void Start () {
        gManager = GameObject.Find("GameManager");
        gm = gManager.GetComponent<GameManager>();
        playerOneScore = gm.pStats.firstPlayerScore;
        playerTwoScore = gm.pStats.secondPlayerScore;
        highScore = highScores[2];
    }

	// Update is called once per frame
	void Update () {
        if (playerOneScore > this.highScore )
        {
            highScores.Add(score);
        }
        if (playerTwoScore > this.highScore)
        {
            highScores.Add(score);
        }

        highScores.Sort(); // sort the scores List
        highScores.Reverse();
        highScores = highScores.GetRange(0, 3);
    }

    void setScore1Text()
    {
        highScore1Text.text = "High Score 1: " + highScores[0].ToString();
    }

    void setScore2Text()
    {
        highScore1Text.text = "High Score 2: " + highScores[1].ToString();
    }

    void setScore3Text()
    {
        highScore1Text.text = "High Score 3: " + highScores[2].ToString();
    }
}
