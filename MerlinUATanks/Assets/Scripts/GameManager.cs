﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    //has the game manager be a static instance
    public static GameManager instance;
    //connects to the tankdata
    [HideInInspector] public TankData data;
    //controls the input controller and keeps track of the player scores
    public InputController player;
    public float playerScore;
    public float enemyScore;
    public float enemyTank;
    public List<GameObject> powerUpsSpawned;
    [HideInInspector] public Transform tf;
    public int columnSize;
    public int numEnemyTanks;
    public bool multiPlayer;
    public bool mapOfTheDay;
    public bool randomMap;
    public Slider volumeSlider;
    public Slider sfxSlider;
    public AudioSource volumeAudio;
    public AudioSource sfxAudio;
    public List<AudioClip> audioClips;
    public AudioClip buttonPress;

    public Text columnText;
    public Text numEnemyText;
    public Text volumeAudioText;
    public Text sfxAudioText;

    public int numberOfPlayers;

    [HideInInspector] public SaveManager sm;
    [HideInInspector] public ScoreData sd;
    [HideInInspector] public PlayersStats pStats;
    [HideInInspector] public GameManager gm;

    // Use this for initialization
    void Awake () {
        //if there is more then one gamemanager it destroys and extras
		if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy (gameObject);
        }
	}
    void Start()
    {
        buttonPress = audioClips[0];
        sd = GetComponent<ScoreData>();
        sm = GetComponent<SaveManager>();
        pStats = GetComponent<PlayersStats>();
        gm = GetComponent<GameManager>();
        //points somewhere
        tf = GetComponent<Transform>();
        data = GetComponent<TankData>();
        //volumeAudio = GetComponent<AudioSource>();
        //sets these voids into effect
        setColumnText();
        setNumEnemyText();
        setVolumeAudioTextText();
    }
    // Update is called once per frame
    void Update () {
        if ( multiPlayer = true)
        {
            numberOfPlayers = 2;
        }
        else if (multiPlayer = false)
        {
            numberOfPlayers = 1;
        }
        //if there are 0 players left quit the game
        if (numberOfPlayers <= 0)
        {
            SceneManager.LoadScene(2);
            //Application.Quit();
        }
    }

    //sets players to single player
    public void SinglePlayer()
    {
        multiPlayer = false;
    }

    //sets players to multiplayer
    public void MultiPlayer()
    {
        multiPlayer = true;
    }
    //adds one to the enemy counter
    public void AddEnemy()
    {
        numEnemyTanks = numEnemyTanks + 1;
        setNumEnemyText();

    }
    //removes one to the enemy counter
    public void RemoveEnemy()
    {
        numEnemyTanks = numEnemyTanks - 1;
        setNumEnemyText();
    }
    //adds one to the column counter
    public void AddColumn()
    {
        columnSize = columnSize + 1;
        setColumnText();
    }
    //removes one from the column counter
    public void RemoveColumn()
    {
        columnSize = columnSize - 1;
        setColumnText();
    }
    //makes the map generate a randim map
    public void RandomMapOn()
    {
        randomMap = true;

    }
    //sets the random map bool to false
    public void RandomMapOff()
    {
        randomMap = false;

    }
    //sets the map to a map of the day
    public void MapoftheDayOn()
    {
        mapOfTheDay = true;
    }
    //sets the motd bool to false
    public void MapoftheDayOff()
    {
        mapOfTheDay = false;
    }
    //sets the music volume to a slider and prints the number as text
    public void VolumeController()
    {
        volumeAudio.volume = volumeSlider.value;
        setVolumeAudioTextText();
    }
    //sets the sfx volume to a slider and prints the number as text
    public void SFXController()
    {
        sfxAudio.volume = sfxSlider.value;
        setSFXAudioTextText();
    }
    //sets the column number as text
    void setColumnText()
    {
        columnText.text = "Columns: " + columnSize.ToString();
    }
    //sets the enemy number as text
    void setNumEnemyText()
    {
        numEnemyText.text = "Number of Enemy Tanks: " + numEnemyTanks.ToString();
    }
    //sets the music volume number as text
    void setVolumeAudioTextText()
    {
        volumeAudioText.text = "Music Volume: " + volumeSlider.value.ToString();
    }
    //sets the sfx volume number as text
    void setSFXAudioTextText()
    {
        sfxAudioText.text = "SFX Volume: " + sfxSlider.value.ToString();
    }

    public void buttonPressSound()
    {
        sfxAudio.PlayOneShot(buttonPress, sfxAudio.volume);
    }
}
