﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Powerup {

    // Powerup Data
    //the amount added
    public float bonusAmount;
    //how long till the effect wears off
    public float countdown;
    //whether the effect is perment or not
    public bool isPerm;

    // Constructor
    //makes the powerup class with the following data
    public Powerup() { }
    //creates a cloned powerup with all the same stats as the child when used
    public Powerup (Powerup powerupToClone)
    {
        //clones the following amounts into a cloned powerup
        bonusAmount = powerupToClone.bonusAmount;
        countdown = powerupToClone.countdown;
        isPerm = powerupToClone.isPerm;
    }

    //creates two voids that can be overrriden in any child classes
    public virtual void OnAddPowerup( TankData data ) { }
    public virtual void OnRemovePowerup( TankData data ) { }
}
