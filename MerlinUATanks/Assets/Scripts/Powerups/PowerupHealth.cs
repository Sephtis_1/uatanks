﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PowerupHealth : Powerup {
    //overides the void OnAddPowerup in the powerup parent class
    public override void OnAddPowerup(TankData data)
    {
        // Add to health by the set amount
        data.health.currentHealth += bonusAmount;
        //inherites from the parent void
        base.OnAddPowerup(data);
    }
    //overides the void OnRemovePowerup in the powerup parent class
    public override void OnRemovePowerup(TankData data)
    {
        //does nothing if it is perment
        if (isPerm)
            return;
        //inherites from the parent void
        base.OnRemovePowerup(data);

        // Remove from health the set amount
        data.health.currentHealth -= bonusAmount;
    }
}
