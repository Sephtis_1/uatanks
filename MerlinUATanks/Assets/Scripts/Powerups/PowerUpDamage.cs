﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PowerUpDamage : Powerup
{
    //overides the void OnAddPowerup in the powerup parent class
    public override void OnAddPowerup(TankData data)
    {
        // subtracts the set amount from the effected tank
        data.health.currentHealth -= bonusAmount;
        //inherits from the parent void
        base.OnAddPowerup(data);
    }
    //overides the void OnRemovePowerup in the powerup parent class
    public override void OnRemovePowerup(TankData data)
    {
        //does nothing if it is perment
        if (isPerm)
            return;
        //inherits from the parent void
        base.OnRemovePowerup(data);

        // adds the subtracted health to the effected tank
        data.health.currentHealth += bonusAmount;
    }
}