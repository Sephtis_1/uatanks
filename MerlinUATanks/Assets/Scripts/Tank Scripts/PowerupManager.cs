﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupManager : MonoBehaviour {

    //lists of available powerups/ones effecting the tank
    public List<Powerup> powerups;
    // allows it to access the tank data
    private TankData data;

	// Use this for initialization
	void Start () {
        //gets the tank data on the tank the manager is on
        data = GetComponent<TankData>();		
	}
	
	// Update is called once per frame
	void Update () {
        //starts a new list of powerups that need to be removes eventually
        List<Powerup> powerupsToRemove = new List<Powerup>();
        
        //goes through each powerup and starts to remove from their time each framdraw
       foreach (Powerup powerup in powerups)
        {
            // Subtract last framedraw time (to countdown timer)
            powerup.countdown -= Time.deltaTime;
            // If it <= 0 then remove the powerup
            if (powerup.countdown <= 0)
            {
                //puts the components that have hit zero in the list to be removed
                powerupsToRemove.Add(powerup);
            }
        }
        // Now that foreach is done, we can remove the items 
        foreach (Powerup powerup in powerupsToRemove)
        {
            //removes the powerups from the removal list and removing their lsit
            RemovePowerup(powerup);
        } 
    }

    //takes the powerups when ran into and adds their effects to the tanks data and adds it onto a list
    //of powerups effecting the tank
    public void AddPowerup (Powerup powerupToAdd) {
        Powerup newPowerup = new Powerup(powerupToAdd);
        powerups.Add(newPowerup);
        powerupToAdd.OnAddPowerup(data);
    }

    //removes the powerup and their effects from the tank after their time limit is done.
    public void RemovePowerup(Powerup powerupToRemove) {
        powerups.Remove(powerupToRemove);
        powerupToRemove.OnRemovePowerup(data);
    }

}
