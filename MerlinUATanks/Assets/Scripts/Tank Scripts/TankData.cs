﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TankMover))]
[RequireComponent(typeof(TankShooter))]

public class TankData : MonoBehaviour
{
    //allows the editors to set the move and turn speeds
    public float moveSpeed;
    public float turnSpeed;
    //allows the editor to set the score value the object this is equipped to for scoring purposes
    public float pointValue;
    //noisemaker
    public float volume;
    //score
    public float score;
    //bullet peices
    public float shootRate;
    public float bulletDamage;
    public float bulletSpeed;
    public GameObject bulletPrefab;

    // Other Tank Components
    [HideInInspector] public TankMover mover;
    [HideInInspector] public TankShooter shooter;
    [HideInInspector] public EnemyShooter enemyShooter;
    [HideInInspector] public TankData data;
    [HideInInspector] public Health health;
    [HideInInspector] public Bullet bullet;
    [HideInInspector] public PlayerScores scores;
    [HideInInspector] public InputController ic;

    void Start()
    {
        //sets the components to other scripts equipped to the tank
        mover = GetComponent<TankMover>();
        shooter = GetComponent<TankShooter>();
        enemyShooter = GetComponent<EnemyShooter>();
        data = GetComponent<TankData>();
        health = GetComponent<Health>();
        bullet = GetComponent<Bullet>();
        scores = GetComponent<PlayerScores>();
        ic = GetComponent<InputController>();
    }

    void Update()
    {
    }
}
